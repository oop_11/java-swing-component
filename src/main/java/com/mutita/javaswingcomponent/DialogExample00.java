/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mutita.javaswingcomponent;

/**
 *
 * @author Admin
 */
import javax.swing.*;  
import java.awt.*;  
import java.awt.event.*;  
import javax.swing.JButton;

public class DialogExample00 {  
    private static JDialog d;  
    DialogExample00() {  
        JFrame f= new JFrame();  
        d = new JDialog(f , "Dialog Example", true);  
        d.setLayout( new FlowLayout() );  
        JButton b = new JButton ("OK");  
        b.addActionListener (new ActionListener()  
        {  
            public void actionPerformed( ActionEvent e )  
            {  
                DialogExample00.d.setVisible(false);  
            }  
        });  
        d.add( new JLabel ("Click button to continue."));  
        d.add(b);   
        d.setSize(300,300);    
        d.setVisible(true);  
    }  
    public static void main(String args[])  
    {  
        new DialogExample00();  
    }  
}  

