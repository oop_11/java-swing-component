/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mutita.javaswingcomponent;

/**
 *
 * @author Admin
 */
import javax.swing.*;

public class JavaJSlider00 extends JFrame {

    public JavaJSlider00() {
        JSlider slider = new JSlider(JSlider.HORIZONTAL, 0, 50, 25);
        JPanel panel = new JPanel();
        panel.add(slider);
        add(panel);
    }

    public static void main(String s[]) {
        JavaJSlider00 frame = new JavaJSlider00();
        frame.pack();
        frame.setVisible(true);
    }
}
