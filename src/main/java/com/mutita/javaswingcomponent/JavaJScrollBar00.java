/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mutita.javaswingcomponent;

/**
 *
 * @author Admin
 */
import javax.swing.*;

class JavaJScrollBar00 {

    JavaJScrollBar00() {
        JFrame f = new JFrame("Scrollbar Example");
        JScrollBar s = new JScrollBar();
        s.setBounds(100, 100, 50, 100);
        f.add(s);
        f.setSize(400, 400);
        f.setLayout(null);
        f.setVisible(true);
    }

    public static void main(String args[]) {
        new JavaJScrollBar00();
    }
}

